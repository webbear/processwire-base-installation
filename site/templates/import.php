<?php
// if (file_exists('concerts.xml')) {
	$xml = simplexml_load_file('http://hansjakob-bollinger.ch/site/export');
	
	foreach ($xml->item as $item) {
		// $body .= $item->title . "<br>";
// 		$body .= $item->time . "<br>";
// 		$body .='<div>'. trim($item->text) . '</div><hr>';
		$event = new Page();
		$event->parent = $pages->get("/diary/");
		$event->template = $templates->get('calendar_entry');
		$event->title = $item->title;
		$event->event_date = $item->time;
		$event->event_custom_date = $item->time_custom;
		$event->event_place = $item->city;
		$event->body = trim($item->text);
		$event->name = substr($item->time, 0, -6)."-".$item->title;
		$name = $event->name; 
		// a counter incremented each time a duplicate is found
		$n = 0; 
		// find the first non-duplicate name
		while(count($event->parent->children("name=$name")) > 0) {
		    $n++; 
		    $name = $event->name . $n; // i.e. sears-tower1, sears-tower2, etc.
		}
		// set the page's name to be one we know is unique, i.e. sears-tower1
		$event->name = $name;
		
		$event->save();
	}
// } else {
// 	$body .= "sorry, no file found";
// }